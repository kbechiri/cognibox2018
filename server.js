var express = require("express");
var https = require('https');
var bodyParser = require("body-parser");
var path = require('path');
var fs = require('fs');
var app = express();

// Mongo db access
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';

// HTTPS
var privateKey  = fs.readFileSync('911_User/ssl/server.key', 'utf8');
var certificate = fs.readFileSync('911_User/ssl/server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate}; // Credentials
var httpsServer = https.createServer(credentials, app);

app.use(express.static(path.join(__dirname + '/')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Genrate code session
// Alphanumeric string generator (length of string)
function randomString(length) {
    var result = '';
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function generateSessionCode()
{
	var sessionCode = randomString(32);
  	console.log(sessionCode);
    return {'session code' : sessionCode}; // Session code in JSON
}

// Recurrent function to feed agent map.
function reccurent(res){
  setInterval(function(){
    if(lastGeo != geo){
      res.write('data: ' + JSON.stringify(geo) + '\n\n')
      lastGeo = geo;
    }
    reccurent(res);
  }, 1000);
}

function Create(callback) {
  let geo = {};
  let resend = undefined;
  return {
    getGeo: function(res) {
      callback(re, geo)
    },
    setGeo: function(g) {
      geo = g;
      if(resend){
      callback(resend, geo);}
    },
    setResend: function(resend_in) {
      resend = resend_in;
    }
  }
}
let geoInstance = Create(function(res, geo) {
  res.write('data: ' + JSON.stringify(geo) + '\n\n')
});

// let coords = {"x": -72.7310032, "y": 46.5776159}
let geo = {};
let lastGeo = {};

// Express routing

// Gather geolocalisation.
app.get("/call",
  function(req, res){
    //lastGeo = geo;
    geo = {lat: req.param('lng'), lng: req.param('lat')};
    geoInstance.setGeo(geo);
    console.log(geo);

    var sessioncode = generateSessionCode(); // Generate code randomly
    geo["id"] = sessioncode['session code'];
    geo['time'] = new Date();

    MongoClient.connect(url, function(err, db) {
      console.log("Connected successfully to server");

      var dbm = db.db("cognibox");

      dbm.collection("geolocalisation").insertOne(geo, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
      });

      db.close();
    });
  }
);

// Send last geolocalisation.
app.get("/newgeo",
  function(req, res){
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(geo));

    MongoClient.connect(url, function(err, db) {
      console.log("Connected successfully to server");

      var dbm = db.db("cognibox");

      dbm.collection("geolocalisation").findOne({}, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
      });

      db.close();
    });
  }
);

// Save last.
app.post("/savedata",
  function(req, res){
    var data = req.body;
    console.log(JSON.stringify(data));

    MongoClient.connect(url, function(err, db) {
      console.log("Connected successfully to server");

      var dbm = db.db("cognibox");

      dbm.collection("dossiers").insertOne(data, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
      });

      db.close();
    });

    res.send(JSON.stringify(data));
  }
);

app.get('/api/position', function(req, res) {
  console.log("KLL")
  console.log(geo)
  res.header("Content-Type", "text/event-stream");
  // res.header("Access-Control-Allow-Origin", "*")
  res.header('Cache-Control', 'no-cache')
  //ccurent(res);
  geoInstance.setResend(res)

  // res.send("data: MON TEST")
  // res.end()
});


app.listen(8080);
httpsServer.listen(8443); // Port listening
console.log("Listening on 8443")
