// ----------------------------------------------------- ASSOCIATION LEAFLET D3 --------------------------

let host = d3.select("#map");
host
  .style({
    // 'width': '100%',
    // 'height': '100%'
    'width': '100%',
    'height': '600px',
    'margin': 'auto'
  });

let copyright = 'Tiles courtesy of <a href=\'http://openstreetmap.se/=\' target=\'_blank\'>' + 'OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href=\'http://www.openstreetmap.org/copyright\'>OpenStreetMap</a>';
let url = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
// let url = "http://{s}.tile.openstreetmap.se/hydda/base/{z}/{x}/{y}.png";
// let map = linkDivToLeaflet(url);
var map = L.map('map', { worldCopyJump: true })
           .setView([46.5623598, -72.7408895], 12);
L.tileLayer(url, {
  attribution: copyright,
  maxZoom: 16
}).addTo(map);

// let [svg, g] = linkD3toLeaflet(map);
var svg = d3.select(map.getPanes().overlayPane).append("svg");
var g = svg.append("g").attr("class", "leaflet-zoom-hide");

let casernes = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "name": "Caserne grand mere"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-72.6964373, 46.6134848]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "name": "Caserne Shawinigan"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-72.7456407, 46.5543717]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "name": "Poste Saint-Georges-de-Champlain"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-72.6585197, 46.6261639]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "name": "Poste Shawinigan-Sud"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-72.7184217, 46.5075617]
      }
    }
  ]
}

d3.json('data/dea_defibrillateurs.geojson', function(e1, d1) {
  d3.json('data/aerobases.geojson', function(e2, d2) {
    d3.json('data/quebec.geojson', function(e3, d3) {
      ready(e1 || e2 || e3, d1, d2, d3);
    });
  });
});

var transform = d3.geo.transform({point: projectPoint});
var path = d3.geo.path()
  .projection(transform)
  .pointRadius(5)

let urgenceTip = d3
  .tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function() {
    var current = d3.select(this);
    return 'Appel entrant...<br><br><button class="btn btn-lg btn-primary big-btn" onclick="answer()">Répondre</button>'
  });
let isHidden = true;

let d3_urgence = g
  .append("g")
  .selectAll("path")
  // .data([{'geometry': {'type': 'Point', 'coordinates': [-72.7394437, 46.5683207]}}])
  // .data([{'type': 'Feature', 'geometry': {'type': 'Point', 'coordinates': [-72.7394437, 46.5683207]}}])
  .data([])
  .enter()
  .append("path")
  .attr({
    // "fill": 'url(img/defibrillateur.svg)'
    "class": "animate-flicker urgence",
    "fill": "red"
  })


// function ready(error, quebec, routes, aerobases) {
function ready(error, defibrillateurs, aerobases, quebec) {
  if (error) throw error;

  // Découpage du Québec sur d3
  let d3_quebec = g.append("g")
                     .selectAll("path")
                     .data(quebec.features)
                     .enter()
                     .append("path")
                     .attr({
                       "stroke": "black",
                       "stroke-width": 2,
                       "fill": "none"
                     })


  let d3_defibrilateurs = g
    .append("g")
    .selectAll("path")
    .data(defibrillateurs.features)
    .enter()
    .append("path")
    .attr({
      // "fill": 'url(img/defibrillateur.svg)'
      "fill": "blue"
    })

  let d3_casernes = g.append("g")
                        .selectAll("path")
                        .data(casernes.features)
                        .enter()
                        .append("path")
                        .attr({
                          "fill": "black"
                        })

  d3_defibrilateurs.attr("d", path);
  d3_casernes.attr("d", path);
  d3_urgence.attr("d", path);
  d3_quebec.attr("d", path);
  tooltipCaserne(svg, d3_casernes);
  tooltipDefibrilateurs(svg, d3_defibrilateurs);

  map.on("viewreset", update);
  update();

  d3_urgence
    .on("click", function() {
      if (isHidden) {
        urgenceTip.show()
        isHidden = false;
      } else {
        urgenceTip.hide()
        isHidden = true;
      }
    })
    // .on("mouseout", tip.hide);
  svg.on("mousedown", urgenceTip.hide)
  // svg.on("mousedown", tip.hide)

  svg.call(urgenceTip);

  /////////////////////////////
  var source = new EventSource("https://192.168.108.149:8443/api/position")
  source.addEventListener('message', function(e) {
    let coord = JSON.parse(e.data);
    console.log(coord)
    if (coord.lat && coord.lng) {
      g.selectAll(".urgence")
       .remove();

      d3_urgence = g
        // .append("g")
        .selectAll(".urgence")
        // .data([{'geometry': {'type': 'Point', 'coordinates': [-72.7394437, 46.5683207]}}])
        .data([{'type': 'Feature', 'geometry': {'type': 'Point', 'coordinates': [coord.lat, coord.lng]}}])
        .enter()
        .append("path")
        .attr({
          // "fill": 'url(img/defibrillateur.svg)'
          "class": "animate-flicker urgence",
          "fill": "red"
        })

      d3_urgence
        .attr("d", path)
        .on("click", function() {
          if (isHidden) {
            urgenceTip.show()
            isHidden = false;
          } else {
            urgenceTip.hide()
            isHidden = true;
          }
        })
    }
  }, false);

  source.addEventListener('open', function(e) {
    // Connection was opened.
    console.log("OPEN")
  }, false);
  /////////////////////////////

  function update() {
      // Redimensionnement et repositionnement du SVG
      // positionSVG(svg, g, path, d3_batiments);
      bounds = path.bounds(quebec);

      var topLeft = bounds[0],
          bottomRight = bounds[1];

      svg.attr("width", bottomRight[0] - topLeft[0])
         .attr("height", bottomRight[1] - topLeft[1])
         .style("left", topLeft[0] + "px")
         .style("top", topLeft[1] + "px");

      g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

      // Mise à jour élements SVG
      // featuresUpdate(d3_quebec,d3_routes,d3_aerobases, path);
      d3_defibrilateurs.attr("d", path.pointRadius(5));
      d3_casernes.attr("d", path.pointRadius(5));
      d3_urgence.attr("d", path.pointRadius(5));
      d3_quebec.attr("d", path);
  }
}

function answer() {
  document.location.href = "answer.html"
}

// ------------------------------------------ LÉGENDE ----------------------------------------
legende();

/**
 * Associe le fond de carte de l'url à la balise div précedemment créée.
 *
 * @param url Lien vers le fond de carthographie
 */
function linkDivToLeaflet(url) {
  let copyright = 'Tiles courtesy of <a href=\'http://openstreetmap.se/=\' target=\'_blank\'>' +
    'OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href=\'http://www.openstreetmap.org/copyright\'>OpenStreetMap</a>';

  let bounds = new L.LatLngBounds(new L.LatLng(42.0, -85.0), new L.LatLng(60.0, -50.0));
  let layer = L.tileLayer(url, {
    attribution: copyright,
    // maxZoom: 7,
    // minZoom: 4,
    reset: false
  });
  let map = new L.Map('map', {
    zoom: 4.5,
    center: bounds.getCenter(),
    layers: [layer],
    maxBounds: bounds,
    maxBoundsViscosity: 0.75
  });

  setTimeout(() => {
      map.invalidateSize();
      map.fitBounds(bounds);
  }, 300);
  return map;
}

/**
 * Associe D3 à Leaflet.
 *
 * @param map Carte Leaflet
 */
function linkD3toLeaflet(map) {
  let svg = d3.select(map.getPanes().overlayPane).append('svg');
  let g = svg.append('g').attr('class', 'leaflet-zoom-hide');

  return [svg, g];
}
