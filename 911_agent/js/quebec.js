/* Chargement de la carte du Quebec et des différentes informations à afficher
 */
function chargement() {
  /*  TODO
   Charger les données et lancer la fonction "ready" à la fin des chargements
   */
  d3.json('data/quebec.geojson', function(e1, d1) {
    d3.json('data/routes.geojson', function(e2, d2) {
      d3.json('data/aerobases.geojson', function(e3, d3) {
        ready(e1 || e2 || e3, d1, d2, d3);
      })
    })
  });
}

/*  Création des éléments à placer sur le SVG
    quebec : données geoJSON de quebec
    routes : données geoJSON sur les routes du Quebec
    aerobases : données geoJSON sur les bases aériennes du Québec
 */
function d3Features(quebec, routes, aerobases) {
  /*  TODO
      Ajouter des paths pour chacune des données à afficher
   */

  var svg = d3.select("svg g");

  // Découpage du Québec sur d3
  var d3_quebec = svg.append("g")
                     .selectAll("path")
                     .data(quebec.features)
                     .enter()
                     .append("path")
                     .attr({
                       "stroke": "black",
                       "stroke-width": 2,
                       "fill": "none"
                     })

  // Routes du Québec sur d3
  var d3_roads = svg.append("g")
                    .selectAll("path")
                    .data(routes.features)
                    .enter()
                    .append("path")
                    .attr({
                      "stroke": "purple",
                      "fill": "none"
                    });

  // Aéroports du Québec sur d3
  var d3_aerobases = svg.append("g")
                        .selectAll("path")
                        .data(aerobases.features)
                        .enter()
                        .append("path")
                        .attr({
                          "fill": function(d) {
                            if (d.properties.TRP_DE_IND == "Aéroport") {
                              return "red";
                            } else if (d.properties.TRP_DE_IND == "Hydrobase") {
                              return "blue";
                            }
                            return "black";
                          },
                          "data-aeroport": function(d) {
                            return JSON.stringify({
                              "type": d.properties.TRP_DE_IND,
                              "lieu": d.properties.TRP_NM_TOP
                            })
                          },
                        })

  return [d3_quebec, d3_roads, d3_aerobases];
}

/* Redimensionne et repositionne le SVG sur la carte lors d'une update
 */
function positionSVG(svg, g, path, quebec) {

  /*  TODO
   En utilisant la fonction bounds de path, recadrer et redimensionner svg et g
   */
  bounds = path.bounds(quebec);

  var topLeft = bounds[0],
      bottomRight = bounds[1];

  svg.attr("width", bottomRight[0] - topLeft[0])
     .attr("height", bottomRight[1] - topLeft[1])
     .style("left", topLeft[0] + "px")
     .style("top", topLeft[1] + "px");

  g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
}

/* Mise à jour des éléments d3 sur la carte
    d3_quebec : elements path delimitant le Quebec
    d3_roads : elements path des routes du Quebec
    d3_aerobases : elements path des bases aeriennes du Quebec
    path : projection des points sur la carte
 */

function featuresUpdate(d3_quebec, d3_roads, d3_aerobases, path) {

  /*  TODO
   Préciser ou mettre à jour les différents path
   Se référer à la section "Affichage des données" de l'énoncé pour gérer le cas du zoom
   Un zoom est considéré comme très faible lorsqu'il est inférieur ou égal à 3
   */
  console.log(map.getZoom());
  d3_quebec.attr("d", path);
  d3_roads.attr("d", path);
  d3_aerobases.attr("d", path.pointRadius(map.getZoom()));

  if (map.getZoom() < 3) {
    d3_quebec.attr("stroke-width", 1);
    d3_roads.attr("stroke-width", 0);
  } else {
    d3_quebec.attr("stroke-width", 2);
    d3_roads.attr("stroke-width", 1);
  }
}

