function initGeolocation() {
  if (navigator && navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successCallback, errorCallback, {enableHighAccuracy:true});
//    navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
  } else {
    console.log('Geolocation is not supported');
  }
}

function errorCallback() {}

function successCallback(position) {


//  var mapUrl = "http://maps.google.com/maps/api/staticmap?center=";
//  mapUrl = mapUrl + '&markers=color:red%7Clabel:A%7C';
//  mapUrl = mapUrl + position.coords.latitude + ',' + position.coords.longitude;
//  mapUrl = mapUrl + '&zoom=15&size=512x512&maptype=roadmap&sensor=false';
//  var imgElement = document.getElementById("static-map");
//  imgElement.src = mapUrl;

  var crd = position.coords;
  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);

  var obj = {"latitude": crd.latitude, "longitude":crd.longitude, "accuracy":crd.accuracy};

  var gpsInfo = JSON.stringify(obj);
  console.log(gpsInfo);

  $.get(
      "https://192.168.108.149:8443/call",
      {lat : obj["latitude"], lng : obj["longitude"], acc: obj["accuracy"]},
        function(data) {
           alert('page content: ' + data);
        }
    );
}
