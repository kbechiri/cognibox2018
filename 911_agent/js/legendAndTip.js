/* Crée et affiche la légende sur le SVG
 */
function legende() {

  // Création d'un conteneur svg pour afficher la légende associée à la couleur des partis politiques
  var margin = {
          top: 15,
          right: 20,
          bottom: 20,
          left: 10
      },
      width = 240 - margin.left - margin.right,
      height = 160 - margin.top - margin.bottom;

  /*  TODO
   Créer un nouvel élément SVG
   Afficher la légende précisant les différents éléments ajoutés
   Pour les bases aeriennes, vous pouvez utiliser des cercles
   Pour les frontières et les routes; vous pouvez utiliser des lignes
   */
  var svgLegend = d3.select("#map")
                    .append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .attr("class", "legend");

  var groups = svgLegend.append("g");

  groups.append("rect")
        .attr({
          "width": width,
          "height": height,
          "fill": "rgba(255, 222, 173, 0.9)"
        });

  var g = groups.append("g").attr("transform", "translate(" + margin.left + ", " + (2 * margin.top) + ")")
      g.append("text")
       .text("Appel video entrant")
       .attr({ "x": 40, "font-size": 12 })
      g.append("circle").attr({"cx": 10, "cy": -5, "r": 5, "fill": "red"})

      g = groups.append("g").attr("transform", "translate(" + margin.left + ", " + (2 * margin.top + 25) + ")")
      g.append("text")
       .text("Appel transféré")
       .attr({ "x": 40, "font-size": 12 })
      g.append("circle").attr({"cx": 10, "cy": -5, "r": 5, "fill": "green"})

      g = groups.append("g").attr("transform", "translate(" + margin.left + ", " + (2 * margin.top + 50) + ")")
      g.append("text")
       .text("Défibrillateur")
       .attr({ "x": 40, "font-size": 12 })
      g.append("circle").attr({"cx": 10, "cy": -5, "r": 5, "fill": "blue"})

      g = groups.append("g").attr("transform", "translate(" + margin.left + ", " + (2 * margin.top + 75) + ")")
      g.append("text")
       .text("Caserne")
       .attr({ "x": 40, "font-size": 12 })
      g.append("circle").attr({"cx": 10, "cy": -5, "r": 5, "fill": "black"})
}

/* Création de la toolbox et ajout des eventListener aux paths
 svg : SVG de la carte
 aerobases : ensemble des éléments path représentant les différentes bases aeriennes
 */
function tooltipCaserne(svg, caserne) {

  /*  TODO
   La toolTip doit contenir :
   le lieu de la base aérienne
   le type de la base aérienne (aéroport ou hydrobase)

   Rajouter les eventListeners de la toolTip à aerobases
   */
  var tip = d3.tip()
              .attr('class', 'd3-tip')
              .offset([-10, 0])
              .html(function(d) {
                var current = d3.select(this);
                var infos = JSON.parse(current.attr("Feature"));
               return "Caserne: " + d.properties.name;
              });

  caserne.on("mouseover", tip.show)
         .on("mouseout", tip.hide);

  svg.call(tip);
}

function tooltipDefibrilateurs(svg, caserne) {
  var tip = d3.tip()
              .attr('class', 'd3-tip')
              .offset([-10, 0])
              .html(function(d) {
                var current = d3.select(this);
                var infos = JSON.parse(current.attr("Feature"));
                return "Defibrilateur </br> Source: " + d.properties.Source + "</br>" +
                      "Adresse: " + d.properties.Adresse+ "</br>" +
                      "Bâtiments: " + d.properties.Bâtiments + "</br>" +
                      "Modèle: " + d.properties.Modèle;
              });

  caserne.on("mouseover", tip.show)
         .on("mouseout", tip.hide);

  svg.call(tip);
}
