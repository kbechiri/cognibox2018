var express = require("express");
var https = require('https');
var fs = require('fs');
var app = express();
var path = require('path');

var privateKey  = fs.readFileSync('ssl/server.key', 'utf8');
var certificate = fs.readFileSync('ssl/server.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var httpsServer = https.createServer(credentials, app);

app.use(express.static(path.join(__dirname + '/')));

app.get("/call", function(req, res){
  var geo = {lat: req.param('lat'), lng: req.param('lng')};
  console.log(geo)
  }
);

app.listen(8080);
httpsServer.listen(8443);
